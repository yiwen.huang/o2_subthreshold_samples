# O2_subthreshold_samples

O2 sub-threshold samples for comparison. For large posterior samples in `.gz` format, unzip with `gunzip [file.gz]`.

[Checks Done and Plots](https://git.ligo.org/yiwen.huang/o2_subthreshold_samples/blob/master/ChecksDone.md)


## Posterior Samples
Files in the format of `[GW]_[Approximant]_posterior_samples.dat` contain the full posterior samples, including but not limited to these:

* `logl`: log likelihood

* `mc` or `mc_source`: chirp mass

* `eta`: mass ratio

* `a1z`: aligned spin of primary

* `a2z`: aligned spin of secondary

* `ra`: right ascension

* `dec`: declination

* `psi`: polarization angle

* `theta_jn`: inclination

* `phase`: orbital phase

* `time`: arrival time at each IFO

* `distance` or `dist`: luminosity distance
    
### Example Codes


```
import numpy
pos=numpy.genfromtxt('GW170727_SEOBNRv4_ROMpseudoFourPN_posterior_samples.dat',names=True)
eta=pos['pos']
```
One may also use `panda` or other packages one prefers. 

## Power Spectral Density (PSD) for each interferometer(IFO) 
The posterior samples and power spectral density are stored in the format of an ASCII file. 

Files in the format of `GW[trigger]_[PSDlength]_[IFO]_PSD.dat` contain BW PSDs of 4s and 64s, first column is frequency (Hz), second column is PSD (1/Hz).

Plots `datadump[trigger]_[PSDlength]_[IFO].dat` are comparison plots that include (if any) PSDs and frequency data dump (i.e. frequency domain data) for the same segment length.
