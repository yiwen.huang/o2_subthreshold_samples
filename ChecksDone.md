# Same flat-in-chi_eff prior
The following does not prove to have effect
* PSD method (BW vs. Welch + PSD drifting)
* Seglen (4s. vs. 128s)
* Beginning time of seglen (trigtime -60, -45, -30, -15)
* Sampler (LAL vs. IAS)
* Marginalize over calibration envelope (With vs. Without)

[3-param corner plot](https://git.ligo.org/yiwen.huang/o2_subthreshold_samples/blob/master/GW151216/GW151216_comp.png), [full corner plot](https://git.ligo.org/yiwen.huang/o2_subthreshold_samples/blob/master/GW151216/GW151216_comp_full.png)

# Residue Plot ([20,50] and [20,512] Hz)
* Frequency domain 128s data (begin tc - 60s), subtracted with the WF with highest likelihood from IAS using IMRPhenomD, then whitened with IAS PSD
    * Follows N(0,1) with visible tails
    * [(20,50)Hz, H, residue plot](https://git.ligo.org/yiwen.huang/o2_subthreshold_samples/blob/master/GW151216/All_residue151216_H.png)
    * [(20,50)Hz, L, residue plot](https://git.ligo.org/yiwen.huang/o2_subthreshold_samples/blob/master/GW151216/All_residue151216_L.png)
* Frequency domain 4s data (begin tc - 2s), subtracted with the WF with highest likelihood from MIT using SEOBNRv4, then whitened with BW PSD
    * Follows N(0,1)
    * [(20,512)Hz, H, residue plot](https://git.ligo.org/yiwen.huang/o2_subthreshold_samples/blob/master/GW151216/All_residue151216_H_20_512.png)
    * [(20,512)Hz, L, residue plot](https://git.ligo.org/yiwen.huang/o2_subthreshold_samples/blob/master/GW151216/All_residue151216_L_20_512.png)
